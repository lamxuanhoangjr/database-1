CREATE TRIGGER after_insert_chuong
AFTER INSERT ON public.chuong
FOR EACH ROW
EXECUTE FUNCTION delete_logchuong();

CREATE TRIGGER after_delete_chuong
AFTER DELETE ON public.chuong
FOR EACH ROW
EXECUTE FUNCTION insert_logchuong();