CREATE TRIGGER before_insert_dangkylophoc
BEFORE INSERT ON public.dangkylophoc
FOR EACH ROW
EXECUTE FUNCTION limit_dangkylophoc();

CREATE TRIGGER after_insert_dangkylophoc
AFTER INSERT ON public.dangkylophoc
FOR EACH ROW
EXECUTE FUNCTION delete_logdangkylophoc();

CREATE TRIGGER after_delete_dangkylophoc
AFTER DELETE ON public.dangkylophoc
FOR EACH ROW
EXECUTE FUNCTION insert_logdangkylophoc();