CREATE TABLE public.lophoc(
    lophoc_id    INT PRIMARY KEY,
    lophoc_name  TEXT
);

CREATE TABLE public.monhoc(
    monhoc_id   INT PRIMARY KEY,
    lophoc_id   INT,
    monhoc_name TEXT DEFAULT '',
    CONSTRAINT fk_lophoc_cho_monhoc
        FOREIGN KEY (lophoc_id)
        REFERENCES lophoc(lophoc_id)
);

CREATE TABLE public.dethimonhoc(
    dethimonhoc_id        INT PRIMARY KEY,
    monhoc_id             INT,
    dethimonhoc_name      TEXT,
    dethimonhoc_real      BOOLEAN DEFAULT false,
    dethimonhoc_questions INT CHECK (dethimonhoc_questions >= 1) DEFAULT 1,
    dethimonhoc_time      INT CHECK (dethimonhoc_time >= 1) DEFAULT 1,
    CONSTRAINT fk_monhoc_cho_dethimonhoc
        FOREIGN KEY (monhoc_id)
        REFERENCES monhoc(monhoc_id)
);

CREATE TABLE public.thoigiandethichinh(
    thoigiandethichinh_id   INT PRIMARY KEY,
    dethimonhoc_id          INT,
    thoigiandethichinh_time TIMESTAMP WITH TIME ZONE,
    CONSTRAINT fk_dethimonhoc_cho_thoigiandethichinh
        FOREIGN KEY (dethimonhoc_id)
        REFERENCES dethimonhoc(dethimonhoc_id)
);

CREATE TABLE public.chuong(
    chuong_id     INT PRIMARY KEY,
    monhoc_id     INT,
    chuong_number INT DEFAULT 0,
    chuong_name   TEXT DEFAULT '',
    CONSTRAINT fk_monhoc_cho_chuong
        FOREIGN KEY (monhoc_id)
        REFERENCES monhoc(monhoc_id)
);

CREATE TABLE public.cauhoi(
    cauhoi_id    INT PRIMARY KEY,
    chuong_id    INT,
    cautraloi_id INT,
    cauhoi_name  TEXT DEFAULT '',
    CONSTRAINT fk_chuong_cho_cauhoi
        FOREIGN KEY (chuong_id)
        REFERENCES chuong(chuong_id)
);

CREATE TABLE public.cautraloi(
    cautraloi_id   INT PRIMARY KEY,
    cauhoi_id      INT,
    cautraloi_name TEXT DEFAULT '',
    CONSTRAINT fk_cauhoi_cho_cautraloi
        FOREIGN KEY (cauhoi_id)
        REFERENCES cauhoi(cauhoi_id)
);

ALTER TABLE cauhoi ADD
    CONSTRAINT fk_cautraloi_cho_cauhoi
    FOREIGN KEY (cautraloi_id)
    REFERENCES cautraloi(cautraloi_id);

CREATE TABLE public.quyen(
    quyen_id   INT PRIMARY KEY,
    quyen_name TEXT
);

CREATE TABLE public.nguoidung(
    nguoidung_account     TEXT PRIMARY KEY,
    quyen_id              INT,
    nguoidung_password    TEXT,
    nguoidung_displayname TEXT DEFAULT '',
    CONSTRAINT fk_quyen_cho_nguoidung
        FOREIGN KEY (quyen_id)
        REFERENCES quyen(quyen_id)
);

CREATE TABLE public.sinhvien(
    sinhvien_id       INT PRIMARY KEY,
    nguoidung_account TEXT UNIQUE,
    sinhvien_name     TEXT,
    sinhvien_gender   TEXT CHECK (sinhvien_gender='Nam' OR sinhvien_gender='Nữ'),
    sinhvien_birthday TIMESTAMP WITH TIME ZONE,
    CONSTRAINT fk_nguoidung_cho_sinhvien
        FOREIGN KEY (nguoidung_account)
        REFERENCES nguoidung(nguoidung_account)
);

CREATE TABLE public.dangkylophoc(
    dangkylophoc_id INT PRIMARY KEY,
    sinhvien_id     INT,
    lophoc_id       INT,
    CONSTRAINT fk_sinhvien_cho_dangkylophoc
        FOREIGN KEY (sinhvien_id)
        REFERENCES sinhvien(sinhvien_id),
    CONSTRAINT fk_lophoc_cho_dangkylophoc
        FOREIGN KEY (lophoc_id)
        REFERENCES lophoc(lophoc_id)
);

CREATE TABLE public.dethi(
    dethi_id        INT PRIMARY KEY,
    dethimonhoc_id  INT,
    sinhvien_id     INT,
    dethi_startdate TIMESTAMP WITH TIME ZONE,
    dethi_enddate   TIMESTAMP WITH TIME ZONE,
    CONSTRAINT fk_dethimonhoc_cho_dethi
        FOREIGN KEY (dethimonhoc_id)
        REFERENCES dethimonhoc(dethimonhoc_id),
    CONSTRAINT fk_sinhvien_cho_dethi
        FOREIGN KEY (sinhvien_id)
        REFERENCES sinhvien(sinhvien_id)
);

CREATE TABLE public.cauhoidethi(
    cauhoidethi_id INT PRIMARY KEY,
    dethi_id       INT,
    cauhoi_id      INT,
    cautraloi_id   INT,
    CONSTRAINT fk_dethi_cho_cauhoidethi
        FOREIGN KEY (dethi_id)
        REFERENCES DeThi(dethi_id),
    CONSTRAINT fk_cauhoi_cho_cauhoidethi
        FOREIGN KEY (cauhoi_id)
        REFERENCES CauHoi(cauhoi_id),
    CONSTRAINT fk_cautraloi_cho_cauhoidethi
        FOREIGN KEY (cautraloi_id)
        REFERENCES cautraloi(cautraloi_id)
);