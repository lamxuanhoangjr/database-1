-- Comment on functions
COMMENT ON FUNCTION getall_chuong_withlimitoffset(
  p_limit INT,
  p_offset INT
) IS 'Lấy các bảng Chuong với limit và offset';

COMMENT ON FUNCTION getall_dethi_on_sinhvien(
    p_id INT,
    p_limit INT,
    p_offset INT
) IS 'Lấy các bảng DeThi phụ thuộc vào bảng SinhVien với limit và offset';

COMMENT ON FUNCTION limit_cautraloi()
IS 'Giới hạn 4 giá trị CauTraloi phụ thuộc vào CauHoi';

-- Comment on procedures
COMMENT ON PROCEDURE delete_cauhoi(p_id INT)
IS 'Xóa bảng CauHoi (xóa luôn các bảng cautraloi phụ thuộc vào cauhoi)';