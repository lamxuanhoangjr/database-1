/* Lấy bảng SinhVien phụ thuộc vào sinhvien_id */
CREATE OR REPLACE FUNCTION get_sinhvien_withaccount(
    p_account TEXT
)
RETURNS TABLE (
    sinhvien_id         INT,
    nguoidung_account   TEXT,
    sinhvien_name       TEXT,
    sinhvien_gender     TEXT,
    sinhvien_birthday   TIMESTAMP WITH TIME ZONE
) AS
$$
BEGIN
    RETURN QUERY
        SELECT
            sv.sinhvien_id,
            sv.nguoidung_account,
            sv.sinhvien_name,
            sv.sinhvien_gender,
            sv.sinhvien_birthday
        FROM SinhVien sv
        WHERE sv.nguoidung_account = p_account;
END;
$$
LANGUAGE plpgsql;

/* Get All with limit and offset */
CREATE OR REPLACE FUNCTION getall_sinhvien_withlimitoffset(
    p_limit INT,
    p_offset INT
)
RETURNS TABLE (
    sinhvien_id         INT,
    nguoidung_account   TEXT,
    sinhvien_name       TEXT,
    sinhvien_gender     TEXT,
    sinhvien_birthday   TIMESTAMP WITH TIME ZONE
) AS
$$
DECLARE
    sinhvien_count INT;
BEGIN
    SELECT COUNT(sv.sinhvien_id) INTO sinhvien_count FROM SinhVien sv;

    IF p_limit < 5 THEN
        p_limit := 5;
    ELSIF p_limit > 50 THEN
        p_limit := 50;
    END IF;

    IF p_offset < 0 THEN
        p_offset := 0;
    ELSIF p_offset >= CEIL(sinhvien_count::NUMERIC / p_limit) THEN
        p_offset := CASE WHEN CEIL(sinhvien_count::NUMERIC / p_limit) > 0
                         THEN CEIL(sinhvien_count::NUMERIC / p_limit) - 1
                         ELSE 0
                    END;
    END IF;

    RETURN QUERY
        SELECT
            sv.sinhvien_id,
            sv.nguoidung_account,
            sv.sinhvien_name,
            sv.sinhvien_gender,
            sv.sinhvien_birthday
        FROM SinhVien sv
        LIMIT p_limit
        OFFSET p_offset*p_limit;
END;
$$
LANGUAGE plpgsql;

/* Tìm kiếm theo sinhvien_name với limit và offset */
CREATE OR REPLACE FUNCTION searchall_sinhvien_on_name(
    p_keyword TEXT,
    p_limit   INT,
    p_offset  INT
)
RETURNS TABLE (
    sinhvien_id         INT,
    nguoidung_account   TEXT,
    sinhvien_name       TEXT,
    sinhvien_gender     TEXT,
    sinhvien_birthday   TIMESTAMP WITH TIME ZONE
) AS
$$
BEGIN
    RETURN QUERY
        SELECT
            sv.sinhvien_id,
            sv.nguoidung_account,
            sv.sinhvien_name,
            sv.sinhvien_gender,
            sv.sinhvien_birthday
        FROM SinhVien sv
        WHERE sv.sinhvien_name ILIKE '%' || p_keyword || '%'
        ORDER BY sv.sinhvien_id
        LIMIT p_limit
        OFFSET p_offset*p_limit;
END;
$$
LANGUAGE plpgsql;