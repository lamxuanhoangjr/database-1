/* Lấy các bảng Chuong phụ thuộc vào chuong_number */
CREATE OR REPLACE FUNCTION getall_chuong_on_number(
    p_number INT
)
RETURNS TABLE (
    chuong_id     INT,
    monhoc_id     INT,
    chuong_number INT,
    chuong_name   TEXT
) AS $$
BEGIN
    RETURN QUERY
        SELECT c.chuong_id, c.monhoc_id, c.chuong_number, c.chuong_name
        FROM Chuong c WHERE c.chuong_number = p_number;
END;
$$ LANGUAGE plpgsql;

/* Lấy các bảng Chuong phụ thuộc vào Monhoc */
CREATE OR REPLACE FUNCTION getall_chuong_on_monhoc(
    p_id INT
)
RETURNS TABLE (
    chuong_id     INT,
    monhoc_id     INT,
    chuong_number INT,
    chuong_name   TEXT
) AS $$
BEGIN
    RETURN QUERY
        SELECT c.chuong_id, c.monhoc_id, c.chuong_number, c.chuong_name
        FROM Chuong c WHERE c.monhoc_id = p_id;
END;
$$ LANGUAGE plpgsql;

/* Lấy các bảng Chuong với limit và offset */
CREATE OR REPLACE FUNCTION getall_chuong_withlimitoffset(
    p_limit INT,
    p_offset INT
)
RETURNS TABLE (
    chuong_id     INT,
    monhoc_id     INT,
    chuong_number INT,
    chuong_name   TEXT
) AS
$$
DECLARE
    chuong_count INT;
BEGIN
    SELECT COUNT(c.chuong_id) INTO chuong_count FROM Chuong c;

    IF p_limit < 5 THEN
        p_limit := 5;
    ELSIF p_limit > 50 THEN
        p_limit := 50;
    END IF;

    IF p_offset < 0 THEN
        p_offset := 0;
    ELSIF p_offset >= CEIL(chuong_count::NUMERIC / p_limit) THEN
        p_offset := CASE WHEN CEIL(chuong_count::NUMERIC / p_limit) > 0
                         THEN CEIL(chuong_count::NUMERIC / p_limit) - 1
                         ELSE 0
                    END;
    END IF;

    RETURN QUERY
        SELECT c.chuong_id, c.monhoc_id, c.chuong_number, c.chuong_name
        FROM Chuong c
        LIMIT p_limit
        OFFSET p_offset*p_limit;
END;
$$
LANGUAGE plpgsql;

/* Lấy tổng giá trị trong bảng Chuong phụ thuộc vào Monhoc */
CREATE OR REPLACE FUNCTION getcount_chuong_on_monhoc(
    p_id INT
)
RETURNS INT AS $$
DECLARE
    chuong_count INT;
BEGIN
    SELECT COUNT(c.chuong_id) INTO chuong_count
    FROM Chuong c WHERE c.monhoc_id = p_id;

    RETURN chuong_count;
END;
$$ LANGUAGE plpgsql;

/* Lấy toàn bộ chuong phụ thuộc vào giá trị keyword, limit và offset */
CREATE OR REPLACE FUNCTION searchall_chuong(
    p_keyword TEXT,
    p_limit   INT,
    p_offset  INT
)
RETURNS TABLE (
    chuong_id     INT,
    monhoc_id     INT,
    chuong_number INT,
    chuong_name   TEXT
) AS
$$
BEGIN
    RETURN QUERY
        SELECT c.chuong_id, c.monhoc_id, c.chuong_number, c.chuong_name
        FROM chuong c
        WHERE c.chuong_name ILIKE '%' || p_keyword || '%'
        ORDER BY c.chuong_id
        LIMIT p_limit
        OFFSET p_offset*p_limit;
END;
$$
LANGUAGE plpgsql;