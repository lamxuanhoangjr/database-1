/* Lấy dữ liệu bảng cauhoidethi phụ thuộc bảng dethi */
CREATE OR REPLACE FUNCTION getall_cauhoidethi_on_dethi(
    p_id INT
)
RETURNS TABLE (
    cauhoidethi_id INT,
    dethi_id       INT,
    cauhoi_id      INT,
    cautraloi_id   INT
) AS
$$
BEGIN
    RETURN QUERY
        SELECT
            chdt.cauhoidethi_id,
            chdt.dethi_id,
            chdt.cauhoi_id,
            chdt.cautraloi_id
        FROM CauHoiDeThi chdt
        WHERE chdt.dethi_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Lấy tổng số cauhoidethi */
CREATE OR REPLACE FUNCTION getcount_cauhoidethi(
    p_id INT
)
RETURNS INT AS
$$
DECLARE
    tong_cauhoidethi INT;
BEGIN
    SELECT COUNT(chdt.cauhoidethi_id) INTO tong_cauhoidethi
    FROM CauHoiDeThi chdt WHERE chdt.dethi_id = p_id;

    RETURN tong_cauhoidethi;
END;
$$
LANGUAGE plpgsql;

/* Lấy tổng số cauhoidethi có đáp án đúng */
CREATE OR REPLACE FUNCTION getcount_cauhoidethi_true(
    p_id INT
)
RETURNS INT AS
$$
DECLARE
    tong_cauhoidethi_true INT;
BEGIN
    SELECT COUNT(chdt.cauhoidethi_id) INTO tong_cauhoidethi_true
    FROM CauHoiDeThi chdt
    INNER JOIN cauhoi ch ON ch.cautraloi_id = chdt.cautraloi_id
    WHERE chdt.dethi_id = p_id;

    RETURN tong_cauhoidethi_true;
END;
$$
LANGUAGE plpgsql;

/* Gọi hàm này khi 1 bảng đề thi được thêm vô */
CREATE OR REPLACE FUNCTION insert_cauhoidethi_on_dethi_insert()
RETURNS TRIGGER AS
$$
BEGIN
    -- Gọi procedure insert_cauhoidethi_random và đưa giá trị cho hàm
    CALL insert_cauhoidethi_random(NEW.dethi_id);
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

/* Get All with limit and offset */
CREATE OR REPLACE FUNCTION getall_cauhoidethi_withlimitoffset(
    p_limit INT,
    p_offset INT
)
RETURNS TABLE (
    cauhoidethi_id INT,
    dethi_id       INT,
    cauhoi_id      INT,
    cautraloi_id   INT
) AS
$$
DECLARE
    cauhoidethi_count INT;
BEGIN
    SELECT COUNT(chdt.cauhoidethi_id) INTO cauhoidethi_count
    FROM CauHoiDeThi chdt;

    IF p_limit < 5 THEN
        p_limit := 5;
    ELSIF p_limit > 50 THEN
        p_limit := 50;
    END IF;

    IF p_offset < 0 THEN
        p_offset := 0;
    ELSIF p_offset >= CEIL(cauhoi_count::NUMERIC / p_limit) THEN
        p_offset := CASE WHEN CEIL(cauhoi_count::NUMERIC / p_limit) > 0
                         THEN CEIL(cauhoi_count::NUMERIC / p_limit) - 1
                         ELSE 0
                    END;
    END IF;

    RETURN QUERY
        SELECT
            chdt.cauhoidethi_id,
            chdt.dethi_id,
            chdt.cauhoi_id,
            chdt.cautraloi_id
        FROM CauHoiDeThi chdt
        LIMIT p_limit
        OFFSET p_offset*p_limit;
END;
$$
LANGUAGE plpgsql;

/* Ngăn chặn việc cập nhập CauHoiDeThi nếu quá thời gian làm bài */
CREATE OR REPLACE FUNCTION prevent_update_cauhoidethi()
RETURNS TRIGGER AS $$
DECLARE
    dethi_enddate       TIMESTAMP;
    value_current_time  TIMESTAMP;
BEGIN
    -- Lấy mốc thời gian đề thi kết thúc và thời gian hiện tại
    SELECT dt.dethi_enddate INTO dethi_enddate FROM dethi dt;
    SELECT NOW() AT TIME ZONE 'Asia/Ho_Chi_Minh' INTO value_current_time;

    -- So sánh 2 mốc thời gian
    IF (value_current_time >= dethi_enddate) THEN
        -- Nếu có bất kỳ sự cố gắng UPDATE nào, raise một exception
        RAISE EXCEPTION 'Updating CauHoiDeThi table is not allowed after time out';
        RETURN NULL; -- Trả về NULL để hủy bỏ thực hiện UPDATE
    ELSE
        RETURN NEW;
    END IF;
END;
$$ LANGUAGE plpgsql;

/* Ngăn chặn việc xóa CauHoiDeThi */
CREATE OR REPLACE FUNCTION prevent_delete_cauhoidethi()
RETURNS TRIGGER AS $$
BEGIN
    -- Nếu có bất kỳ sự cố gắng DELETE nào, raise một exception
    RAISE EXCEPTION 'Deleting CauHoiDeThi table is not allowed';
    RETURN NULL; -- Trả về NULL để hủy bỏ thực hiện DELETE
END;
$$ LANGUAGE plpgsql;