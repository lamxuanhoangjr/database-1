/* Lấy dữ liệu dangkylophoc dựa vào limit và offset */
CREATE OR REPLACE FUNCTION getall_dangkylophoc_withlimitoffset(
    p_limit  INT,
    p_offset INT
)
RETURNS TABLE (
    dangkylophoc_id INT,
    sinhvien_id     INT,
    lophoc_id       INT
) AS
$$
DECLARE
    dangkylophoc_count INT;
BEGIN
    SELECT COUNT(dklh.dangkylophoc_id) INTO dangkylophoc_count
    FROM dangkylophoc dklh;

    IF p_limit < 5 THEN
        p_limit := 5;
    ELSIF p_limit > 50 THEN
        p_limit := 50;
    END IF;

    IF p_offset < 0 THEN
        p_offset := 0;
    ELSIF p_offset >= CEIL(dangkylophoc_count::NUMERIC / p_limit) THEN
        p_offset := CASE WHEN CEIL(dangkylophoc_count::NUMERIC / p_limit) > 0
                         THEN CEIL(dangkylophoc_count::NUMERIC / p_limit) - 1
                         ELSE 0
                    END;
    END IF;

    RETURN QUERY
        SELECT dklh.dangkylophoc_id, dklh.sinhvien_id, dklh.lophoc_id
        FROM dangkylophoc dklh
        ORDER BY dklh.dangkylophoc_id
        LIMIT p_limit
        OFFSET p_offset*p_limit;
END;
$$ LANGUAGE plpgsql;

/* Giới hạn 1 giá trị DangKyLopHoc của 1 LopHoc cho 1 SinhVien */
CREATE OR REPLACE FUNCTION limit_dangkylophoc()
RETURNS TRIGGER AS $$
DECLARE
    sinhvien_count   INT;
    check_quyen_name TEXT;
BEGIN
    -- Đếm số lượng giá trị cho dangkylophoc_id trong DangKyLopHoc
    SELECT COUNT(sinhvien_id) INTO sinhvien_count
        FROM dangkylophoc
        WHERE sinhvien_id = NEW.sinhvien_id;

    -- Kiểm tra nếu số lượng giá trị vượt quá 1, không chấp nhận chèn dữ liệu mới
    IF sinhvien_count >= 1 THEN
        SELECT q.quyen_name INTO check_quyen_name FROM quyen q
            INNER JOIN nguoidung nd ON nd.quyen_id = q.quyen_id
            INNER JOIN sinhvien sv ON sv.nguoidung_account = nd.nguoidung_account
            WHERE NEW.sinhvien_id = sv.sinhvien_id;

        IF TRIM(LOWER(check_quyen_name)) != 'admin' THEN
            RAISE EXCEPTION 'Exceeded maximum limit of 1 entry for dangkylophoc_id %', NEW.dangkylophoc_id;
        END IF;
    END IF;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;