/* Nhập dữ liệu cho bảng log.lophoc */
CREATE OR REPLACE FUNCTION insert_loglophoc()
RETURNS TRIGGER AS
$$
DECLARE
    public_count INT;
    public_last  INT;
    value_last   INT;
BEGIN
    -- Lấy vị trí cuối của bảng public.lophoc
    SELECT LAST_VALUE(public.lophoc.lophoc_id)
        OVER (ORDER BY public.lophoc.lophoc_id DESC)
        INTO public_last FROM public.lophoc;

    -- Lấy giá trị cuối của public.lophoc
    SELECT LAST_VALUE(log.lophoc.lophoc_id) OVER
        (ORDER BY log.lophoc.lophoc_id DESC)
        INTO value_last FROM log.lophoc;

    -- So sánh giá trị tổng và vị trí cuối nếu giá trị tổng bé hơn
    IF (OLD.lophoc_id < public_last) THEN
      -- Thêm dữ liệu
      IF (value_last >= 1) THEN
          INSERT INTO log.lophoc (lophoc_id, public_lophoc_id)
              VALUES (value_last+1, OLD.lophoc_id);
      ELSE
          INSERT INTO log.lophoc (lophoc_id, public_lophoc_id)
              VALUES (1, OLD.lophoc_id);
      END IF;
    END IF;

    RETURN OLD;
END;
$$
LANGUAGE plpgsql;

/* Xóa dữ liệu cho bảng log.lophoc */
CREATE OR REPLACE FUNCTION delete_loglophoc()
RETURNS TRIGGER AS
$$
DECLARE
    log_count            INT;
    log_public_lophoc_id INT;
BEGIN
    SELECT COUNT(log.lophoc.lophoc_id) INTO log_count FROM log.lophoc;

    IF (log_count > 0) THEN
        SELECT public.lophoc.lophoc_id INTO log_public_lophoc_id
            FROM public.lophoc
            INNER JOIN log.lophoc ON public.lophoc.lophoc_id = log.lophoc.public_lophoc_id
            ORDER BY public.lophoc.lophoc_id DESC LIMIT 1;

        DELETE FROM log.lophoc WHERE log.lophoc.public_lophoc_id = log_public_lophoc_id;
    END IF;

    RETURN NEW;
END;
$$
LANGUAGE plpgsql;