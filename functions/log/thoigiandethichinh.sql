/* Nhập dữ liệu cho bảng log.thoigiandethichinh */
CREATE OR REPLACE FUNCTION insert_logthoigiandethichinh()
RETURNS TRIGGER AS
$$
DECLARE
    public_count INT;
    public_last  INT;
    value_last   INT;
BEGIN
    -- Lấy vị trí cuối của bảng public.thoigiandethichinh
    SELECT LAST_VALUE(public.thoigiandethichinh.thoigiandethichinh_id)
        OVER (ORDER BY public.thoigiandethichinh.thoigiandethichinh_id DESC)
        INTO public_last FROM public.thoigiandethichinh;

    -- Lấy giá trị cuối của public.thoigiandethichinh
    SELECT LAST_VALUE(log.thoigiandethichinh.thoigiandethichinh_id) OVER
        (ORDER BY log.thoigiandethichinh.thoigiandethichinh_id DESC)
        INTO value_last FROM log.thoigiandethichinh;

    -- So sánh giá trị tổng và vị trí cuối nếu giá trị tổng bé hơn
    IF (OLD.thoigiandethichinh_id < public_last) THEN
      -- Thêm dữ liệu
      IF (value_last >= 1) THEN
          INSERT INTO log.thoigiandethichinh (thoigiandethichinh_id, public_thoigiandethichinh_id)
              VALUES (value_last+1, OLD.thoigiandethichinh_id);
      ELSE
          INSERT INTO log.thoigiandethichinh (thoigiandethichinh_id, public_thoigiandethichinh_id)
              VALUES (1, OLD.thoigiandethichinh_id);
      END IF;
    END IF;

    RETURN OLD;
END;
$$
LANGUAGE plpgsql;

/* Xóa dữ liệu cho bảng log.thoigiandethichinh */
CREATE OR REPLACE FUNCTION delete_logthoigiandethichinh()
RETURNS TRIGGER AS
$$
DECLARE
    log_count            INT;
    log_public_thoigiandethichinh_id INT;
BEGIN
    SELECT COUNT(log.thoigiandethichinh.thoigiandethichinh_id) INTO log_count FROM log.thoigiandethichinh;

    IF (log_count > 0) THEN
        SELECT public.thoigiandethichinh.thoigiandethichinh_id INTO log_public_thoigiandethichinh_id
            FROM public.thoigiandethichinh
            INNER JOIN log.thoigiandethichinh ON public.thoigiandethichinh.thoigiandethichinh_id = log.thoigiandethichinh.public_thoigiandethichinh_id
            ORDER BY public.thoigiandethichinh.thoigiandethichinh_id DESC LIMIT 1;

        DELETE FROM log.thoigiandethichinh WHERE log.thoigiandethichinh.public_thoigiandethichinh_id = log_public_thoigiandethichinh_id;
    END IF;

    RETURN NEW;
END;
$$
LANGUAGE plpgsql;