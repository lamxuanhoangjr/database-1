/* Nhập dữ liệu cho bảng log.monhoc */
CREATE OR REPLACE FUNCTION insert_logmonhoc()
RETURNS TRIGGER AS
$$
DECLARE
    public_count INT;
    public_last  INT;
    value_last   INT;
BEGIN
    -- Lấy vị trí cuối của bảng public.monhoc
    SELECT LAST_VALUE(public.monhoc.monhoc_id)
        OVER (ORDER BY public.monhoc.monhoc_id DESC)
        INTO public_last FROM public.monhoc;

    -- Lấy giá trị cuối của public.monhoc
    SELECT LAST_VALUE(log.monhoc.monhoc_id) OVER
        (ORDER BY log.monhoc.monhoc_id DESC)
        INTO value_last FROM log.monhoc;

    -- So sánh giá trị tổng và vị trí cuối nếu giá trị tổng bé hơn
    IF (OLD.monhoc_id < public_last) THEN
      -- Thêm dữ liệu
      IF (value_last >= 1) THEN
          INSERT INTO log.monhoc (monhoc_id, public_monhoc_id)
              VALUES (value_last+1, OLD.monhoc_id);
      ELSE
          INSERT INTO log.monhoc (monhoc_id, public_monhoc_id)
              VALUES (1, OLD.monhoc_id);
      END IF;
    END IF;

    RETURN OLD;
END;
$$
LANGUAGE plpgsql;

/* Xóa dữ liệu cho bảng log.monhoc */
CREATE OR REPLACE FUNCTION delete_logmonhoc()
RETURNS TRIGGER AS
$$
DECLARE
    log_count            INT;
    log_public_monhoc_id INT;
BEGIN
    SELECT COUNT(log.monhoc.monhoc_id) INTO log_count FROM log.monhoc;

    IF (log_count > 0) THEN
        SELECT public.monhoc.monhoc_id INTO log_public_monhoc_id
            FROM public.monhoc
            INNER JOIN log.monhoc ON public.monhoc.monhoc_id = log.monhoc.public_monhoc_id
            ORDER BY public.monhoc.monhoc_id DESC LIMIT 1;

        DELETE FROM log.monhoc WHERE log.monhoc.public_monhoc_id = log_public_monhoc_id;
    END IF;

    RETURN NEW;
END;
$$
LANGUAGE plpgsql;