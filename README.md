# database

Database sử dụng PostgreSQL

## Line of Code (LOC: Lượng dòng code)

Sử dụng phần mềm [cloc](https://github.com/AlDanial/cloc) để tính toán 

+ Không tính insert dữ liệu:
```sh
      50 text files.
      50 unique files.                              
       0 files ignored.

github.com/AlDanial/cloc v 1.96  T=0.02 s (3201.2 files/s, 212497.1 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
SQL                             50            298            233           2788
-------------------------------------------------------------------------------
SUM:                            50            298            233           2788
-------------------------------------------------------------------------------
```

+ Tính luôn insert dữ liệu và những file khác:
```sh
      53 text files.
      53 unique files.                              
       0 files ignored.

github.com/AlDanial/cloc v 1.96  T=0.93 s (57.0 files/s, 4279.0 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
SQL                             53            393            250           3334
-------------------------------------------------------------------------------
SUM:                            53            393            250           3334
-------------------------------------------------------------------------------
```