/* Thêm dữ liệu cho bảng cauhoidethi */
CREATE OR REPLACE PROCEDURE insert_cauhoidethi_random(
    p_dethi_id     INT
) AS
$$
DECLARE
    value_last       INT;
    count            INT;
    cauhoi_count     INT;
    cauhoi_questions INT;
    cauhoi_array     INT[] := ARRAY[]::INT[];
    item             INT;
BEGIN
    -- Lấy  giá trị cuối của bảng
    SELECT LAST_VALUE(cauhoidethi_id) OVER (ORDER BY cauhoidethi_id DESC) INTO value_last FROM CauHoiDeThi;

    -- Lấy giới hạn câu hỏi được đặt trong cauhoi_questions
    SELECT dethimonhoc_questions INTO cauhoi_questions FROM DeThiMonHoc;

    -- Lấy số cauhoi có trong monhoc
    SELECT COUNT(cauhoi_id) INTO cauhoi_count FROM getall_cauhoi_on_monhocid(
        -- monhoc_id
        (SELECT mh.monhoc_id FROM DeThi dt
            INNER JOIN DeThiMonHoc dtmh ON dt.dethimonhoc_id = dtmh.dethimonhoc_id
            INNER JOIN MonHoc mh ON dtmh.monhoc_id = mh.monhoc_id
            WHERE dt.dethi_id = p_dethi_id)
    );

    -- Kiểm tra nếu số lượng câu hỏi không bằng hoặc lớn hơn dethimonhoc_questions
    IF (cauhoi_count >= cauhoi_questions) THEN
        -- Lấy Array dưa trên bảng CauHoi đúng với MonHoc
        cauhoi_array := ARRAY (SELECT cauhoi_id FROM getall_cauhoi_on_monhocid(
            -- monhoc_id
            (SELECT mh.monhoc_id FROM DeThi dt
                INNER JOIN DeThiMonHoc dtmh ON dt.dethimonhoc_id = dtmh.dethimonhoc_id
                INNER JOIN MonHoc mh ON dtmh.monhoc_id = mh.monhoc_id
                WHERE dt.dethi_id = p_dethi_id)
        ) ORDER BY RANDOM() LIMIT cauhoi_questions);

        -- Vòng lặp thêm ngẫu nhiên các CauHoi từ cauhoi_first đến cauhoi_last
        FOREACH item IN ARRAY cauhoi_array LOOP
            -- Thêm dữ liệu
            IF (value_last >= 1) THEN
                INSERT INTO CauHoiDeThi (cauhoidethi_id, dethi_id, cauhoi_id)
                    VALUES (value_last+1, p_dethi_id, item);
            ELSE
                INSERT INTO CauHoiDeThi (cauhoidethi_id, dethi_id, cauhoi_id)
                    VALUES (1, p_dethi_id, item);
            END IF;

            -- Kiểm tra trường hợp và cập nhập dữ liệu
            IF (value_last >= 1) THEN
                value_last := value_last + 1;
            ELSE
                value_last := 1;
            END IF;
        END LOOP;
    ELSE
        RAISE EXCEPTION 'COUNT(cauhoi) must be greater than or equal to dethimonhoc_questions';
    END IF;
END;
$$ LANGUAGE plpgsql;

/* Update */
CREATE OR REPLACE PROCEDURE update_cauhoidethi(
    p_id        INT,
    p_dethi_id  INT,
    p_cauhoi_id INT
) AS
$$
BEGIN
    UPDATE cauhoidethi
    SET
        dethi_id = p_dethi_id,
        cauhoi_id = p_cauhoi_id
    WHERE cauhoidethi_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Cập nhập bảng cautraloi_id */
CREATE OR REPLACE PROCEDURE update_cauhoidethi_cautraloi(
    p_id           INT,
    p_cautraloi_id INT
) AS
$$
BEGIN
    UPDATE cauhoidethi
    SET cautraloi_id = p_cautraloi_id
    WHERE cauhoidethi_id = p_id;
END;
$$
LANGUAGE plpgsql;